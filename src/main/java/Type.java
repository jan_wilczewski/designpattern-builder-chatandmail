/**
 * Created by jan_w on 01.10.2017.
 */
public enum Type {
    UNKNOWN, OFFER, SOCIAL, NOTIFICATIONS, FORUM;
}
