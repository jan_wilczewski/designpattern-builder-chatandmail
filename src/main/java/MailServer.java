import java.util.ArrayList;
import java.util.List;

/**
 * Created by jan_w on 01.10.2017.
 */
public class MailServer {

    private List<Client> listaKlientow = new ArrayList<>();


    public void connect (Client client){
        listaKlientow.add(client);
    }

    public void disconnect (Client client){
        listaKlientow.remove(client);
    }

    public void sendMessage (Mail mail, Client sender){
        for (Client client : listaKlientow){
            if (!client.equals(sender)){
                client.readMail(mail);
            }
        }
    }
}
