/**
 * Created by jan_w on 02.10.2017.
 */
public class Main {

    public static void main(String[] args) {

        MailServer server = new MailServer();
        server.connect(new Client("Janusz"));
        server.connect(new Client("Heniek"));
        server.connect(new Client("Marian"));

        server.sendMessage(MailFactory.createNotificationEmail("Kup dziś dwa opakowania wit C, dostaniesz trzecie gratis!"), new Client("esklep"));
        System.out.println();
        server.sendMessage(MailFactory.createWarningMail("Yeti"), new Client("no-reply"));
    }
}
