import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by jan_w on 02.10.2017.
 */
public class MailFactory {

    public static Mail createNotificationEmail(String treść){
        return new Mail.Builder()
                .setCzySzyfr(false)
                .setDataNadania(new Date())
                . setNadawca("no-reply@zdrowysklep.pl")
                .setIsSpam(false)
                .setTresc("Witaj w zdroywm sklepie! Mamy dla Ciebie nową promocję" + treść)
                .createMail();
    }

    public static Mail createWarningMail(String about){
        return new Mail.Builder().setCzySzyfr(false)
                .setDataNadania(new Date())
                . setNadawca("no-reply@zdrowysklep.pl")
                .setIsSpam(false)
                .setTresc("Uwaga, chcemy Cię ostrzec o: " + about)
                .createMail();
    }
}
