import java.util.ArrayList;
import java.util.List;

/**
 * Created by jan_w on 01.10.2017.
 */
public class Client {

    private String name;
    private List<Mail> listaMaili = new ArrayList<>();

    public Client(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void readMail (Mail mail){
        listaMaili.add(mail);
        System.out.println("Klient " + this.getName() + " otrzymał maila: " + mail.getTresc());
    }
}
